Installation de git.exe
=============

git.exe permet l'utilisation de git depuis un ordinateur Windows. Il vient avec une interface en 
ligne de commande "Git Bash" très pratique. Cette invite de 
commandes va nous permettre de générer une paire de clés RSA. git.exe 
est 
nécessaire à [l'installation de 
TortoiseGit](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_TORTOISEGIT.md).

Installation de git
--------------------

Recommandations:
- Privilégier l'installation dans un dossier local type ```C:\Users\julien.naour\AppData\Local\Git\```
- les options par défauts de l'installeur suffises

Suivre ce [lien](https://git-scm.com/download/win)

![1_download](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/1_download.PNG)

Télécharger la version appropriée en théorie 64-bit. Double cliquer sur l'installeur qui devrait se trouver 
dans votre dossier Téléchargements.

![2_execute](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/2_execute.PNG)

Executer l'installeur.

![3_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/3_next.PNG)

Accepter les conditions générales d'utilisation en cliquant sur "Next".

![4_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/4_next.PNG)

Valider les options en cliquant sur "Next".

![5_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/5_next.PNG)

Choisir l'éditeur de texte utilisé par git. Notepad++ est le plus 
simple pour les habitués à Windows. Nano est un éditeur simple en ligne de commande. Cliquer sur "Next".

![6_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/6_next.PNG)

Choisir l'option "Use Git from Git only" puis cliquer sur "Next".

![7_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/7_next.PNG)

Choisir "Use the OpenSSL library" puis cliquer sur "Next".

![8_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/8_next.PNG)

Choisir "Checkout Windows-style, commit Unix-style line endings" puis cliquer sur "Next".

![9_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/9_next.PNG)

Choisir "Use MinTTY" puis cliquer sur "Next".

![10_next](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/10_next.PNG)

Ne pas cliquer sur "Enable sympbolic links" puis cliquer sur "Next".

![11_install](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/11_install.PNG)

Lancer l'installation en cliquant sur "Install".

![12_in_progress](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/12_in_progress.PNG)

Installation en cours...

![13_congratz](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/13_congratz.PNG)

L'instlalation est terminée. Cliquer sur la boîte "Launch Git Bash" puis sur "Finish". Cela permettra de 
directement mettre en place une paire de clés RSA sur votre ordinateur.

Génération d'une paire de clés RSA depuis Git Bash
-------------------------

A la suite d el'installation il est conseillé de directement créer une paire de clés RSA. Si le Git Bash 
n'est pas lancé, il est possible de le retrouver dans le "Menu Démarrer".

![14_menu_demarrer](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/14_menu_demarrer.PNG)

Rechercher "git" ou "Git Bash" et lancer le.

![15_here_comes_git_bash](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/15_here_comes_git_bash.PNG)

Une fenêtre devrait apparaître de ce type.

![16_welcome_to_the_matrix](https://gitlab.com/DREES/tutoriels/raw/master/img/git_bash/16_welcome_to_the_matrix.PNG)

Lancer la commande ```ssh-keygen -t rsa -C "your.email@sante.gouv.fr" -b 4096``` en remplaçant 
```your.email``` par votre adresse mail sante.gouv.fr. Il est possible de faire l'équivalent d'un coller/```Ctrl+V``` dans 
le terminal en 
utilisant les touches ```Shift+Inser``` ou ```Ctrl+Shift+Inser```. La paire de clés sera générée dans votre 
dossier ```.ssh``` à la racine de votre utilisateur courant.
L'ajout d'une passphrase n'est pas nécessaire appuyer sur ```Enter```

git est désormais installé sur votre ordinateur, vous pouvez passer à l'installation de 
[TortoiseGit](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_TORTOISEGIT.md).
